<?php

class CustomerModel
{
    protected $vpg_beskeder;

    protected $vpg_deeplink;

    public function __construct()
    {
        $this->vpg_beskeder = new VPGBeskeder();

        $this->vpg_deeplink = new VPGDeeplink();
    }

    public function getCustomerByBookingId($request)
    {
        global $wpdb;

        $customer = $wpdb->get_row(
            "SELECT * FROM " . $wpdb->prefix . "bookings
            LEFT JOIN " . $wpdb->prefix . "kunder ON " . $wpdb->prefix . "bookings.kunde_id = " . $wpdb->prefix . "kunder.kunde_id
            LEFT JOIN " . $wpdb->prefix . "ejendomme ON " . $wpdb->prefix . "bookings.ejendom_id = " . $wpdb->prefix . "ejendomme.ejendom_id
            LEFT JOIN " . $wpdb->prefix . "noter_booking ON " . $wpdb->prefix . "bookings.booking_id = " . $wpdb->prefix . "noter_booking.note_reg_id
            WHERE " . $wpdb->prefix . "bookings.booking_id = " . $request['booking_id'] . ";"
        );

        if ($customer->ejendom_status == 1) {
            $customer->ejendom_status_text = '1. gangs kunde';
        }
        elseif ($customer->ejendom_status == 2) {
            $customer->ejendom_status_text = 'Flex';
        }
        elseif ($customer->ejendom_status == 3) {
            $customer->ejendom_status_text = 'Fast';
        }
        else {
            $customer->ejendom_status_text = '';
        }

        $customer->cleaning_schedule_text = '';
        if($customer->booking_branche == 2) {
            $fast_plan = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "faste_plan WHERE booking_id = '$customer->booking_id' LIMIT 1;");

            if($fast_plan) {
                $customer->cleaning_schedule_text = '<h4>Rengøringsplan</h4>';
                $customer->cleaning_schedule_text .= $this->vpg_beskeder->rgPlanText($customer->booking_id);
            }
        }

        $customer->note_content = '';
        if ($customer->note_id) {
            $customer->note_content = '<h4>Noter fra kontoret ang&aring;ende denne udf&oslash;rsel</h4>';
            $customer->note_content .= $customer->note;
        }

        $customer->ejendom_husk = nl2br($customer->ejendom_husk);
        $customer->ejendom_crm = nl2br($customer->ejendom_crm);

        // Property Details
        $customer->property_details_text = '';
        switch ($customer->booking_branche) {
            case 1:
                if ($customer->ejendom_husk != '') {
                    $customer->property_details_text .= '<h4>Husk!</h4>';
                    $customer->property_details_text .= nl2br($customer->ejendom_husk);
                }
                if ($customer->ejendom_noegle_kommentar != '') {
                    $customer->property_details_text .= '<h4>Nøglekommentar</h4>';
                    $customer->property_details_text .= nl2br($customer->ejendom_noegle_kommentar);
                }
                if ($customer->ejendom_crm != '') {
                    $customer->property_details_text .= '<h4>CRM ejendomme</h4>';
                    $customer->property_details_text .= nl2br($customer->ejendom_crm);
                }
                break;
            case 2:
                if ($customer->ejendom_husk_ren != '') {
                    $customer->property_details_text .= '<h4>Husk!</h4>';
                    $customer->property_details_text .= nl2br($customer->ejendom_husk_ren);
                }
                if ($customer->ejendom_noegle_kommentar_ren != '') {
                    $customer->property_details_text .= '<h4>Nøglekommentar</h4>';
                    $customer->property_details_text .= nl2br($customer->ejendom_noegle_kommentar_ren);
                }
                if ($customer->ejendom_crm_ren != '') {
                    $customer->property_details_text .= '<h4>CRM ejendomme</h4>';
                    $customer->property_details_text .= nl2br($customer->ejendom_crm_ren);
                }
                break;
            case 3:
                if ($customer->ejendom_husk_byg != '') {
                    $customer->property_details_text .= '<h4>Husk!</h4>';
                    $customer->property_details_text .= nl2br($customer->ejendom_husk_byg);
                }
                if ($customer->ejendom_noegle_kommentar_byg != '') {
                    $customer->property_details_text .= '<h4>Nøglekommentar</h4>';
                    $customer->property_details_text .= nl2br($customer->ejendom_noegle_kommentar_byg);
                }
                if ($customer->ejendom_crm_byg != '') {
                    $customer->property_details_text .= '<h4>CRM ejendomme</h4>';
                    $customer->property_details_text .= nl2br($customer->ejendom_crm_byg);
                }
                break;
            default:
                $customer->property_details_text = '';
                break;
        }

        $customer->get_noter = $this->vpg_beskeder->getNoter(3, $customer->ejendom_id);
        $customer->get_noter_fast = $this->vpg_beskeder->getNoterFast($customer->kunde_id, $customer->ejendom_id, $customer->booking_branche );

        return $customer;
    }

    public function postEndJob($request)
    {
        global $wpdb;

        $booking = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "bookings WHERE booking_id = " . $request['booking_id'] . ";");

        if ($request['faktura'] == 0) {
            $this->vpg_beskeder->opretNote(1, $request['booking_id'], 2, $booking->medarbejder_id, 'Ingen faktura fordi: ' . $request['svar']);
        }

        $data = array(
            'booking_status' => $request['udfort'],
            'booking_faktura' => $request['faktura'],
        );
        $wpdb->update($wpdb->prefix . 'bookings', $data, array( 'booking_id' => $request['booking_id'] ));

        $kunde = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "kunder WHERE kunde_id = '$booking->kunde_id' LIMIT 1;");

        $pris = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "prisdata WHERE pris_kind = 2 AND ejendom_id = '$booking->ejendom_id' AND pris_branche = '$booking->booking_branche';");

        if ($wpdb->num_rows > 0) {
            if (isset($pris->pris_tidsaftale) && $pris->pris_tidsaftale == 0 && $request['faktura'] == 1 && $request['udfort'] == 2) {
                $fag_id = '';
                switch ($booking->booking_branche) {
                    case 1:
                        $fag_id= 'vinduespudsning';
                        break;
                    case 2:
                        $fag_id= 'reng�ring';
                        break;
                    case 3:
                        $fag_id= 'byg';
                        break;
                }

                $medarbejder = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "medarbejdere WHERE medarbejder_id = " . $booking->medarbejder_id. " LIMIT 1;");

                $telefon = $kunde->kunde_tlf;
                $besked = "Hej " .$kunde->kunde_navn .", \n";
                $besked .= "Vi har nu f�rdiggjort vores " . $fag_id. "s service i jeres hjem. \n";
                $besked .= "Vi ser frem til i vil nyde resultatet. ";
                $besked .= "\nMed venlig hilsen \n";
                $besked .= $medarbejder->medarbejder_navn . ' - Gruppen ApS';

                $this->vpg_beskeder->sendSMS($telefon, $besked);
            }
        }

        $ejendom = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "ejendomme WHERE ejendom_id = " . $booking->ejendom_id . " LIMIT 1;");

        if ($wpdb->num_rows > 0) {
            if ($ejendom->ejendom_status == 1 || $ejendom->ejendom_status_ren == 1 || $ejendom->ejendom_status_byg == 1) {
                $ticket_oprettet_til = $this->vpg_deeplink->getAnsvarligId($kunde->kunde_ansvarlig);

                $data_ticket = array(
                    'ticket_navn' =>$ejendom->ejendom_navn ,
                    'ticket_type' => 3,
                    'ticket_kunde_id' => $ejendom->kunde_id,
                    'ticket_vedr' => 5,
                    'ticket_prioritering' => 3,
                    'ticket_oprettet' => time(),
                    'ticket_oprettet_af' => $request['user_id'],
                    'ticket_adresse' => $ejendom->ejendom_adresse,
                    'ticket_booking_id' => $request['booking_id'],
                    'ticket_oprettet_til' => $ticket_oprettet_til
                );

                $wpdb->insert($wpdb->prefix . 'ticket', $data_ticket);
            }
        }
    }

    public function makeNote($request)
    {
        global $wpdb;

        $ticket_errors = array();
        $ticket_kunde_id = '';
        $ticket_tlf = '';
        $ticket_adresse = '';

        $ticket_overskrift = $request['ticket_overskrift'];
        $ticket_note = $request['ticket_note'];
        if (isset($request['kunde_id'])&& $request['kunde_id']!='')
            $ticket_kunde_id = $request['kunde_id'];
        if (isset($request['ticket_tlf'])&& $request['ticket_tlf']!='') {
            $ticket_tlf = $request['ticket_tlf'];
            if (!is_numeric($ticket_tlf)) {
                $ticket_errors['phone1'] = "Telefon skal indeholde 8 cifre!";
            }
            if (strlen($ticket_tlf) != 8) {
                $ticket_errors['phone2'] = "Telefon skal indeholde 8 cifre!";
            }
        }

        if (isset($request['ticket_adresse'])&& $request['ticket_adresse']!='') {
            $ticket_adresse = $request['ticket_adresse'];
        }

        $user_id = wp_get_current_user()->ID;
        $note_ticket_author = get_current_user_id();
        if (isset($request['afsender_ticket_kundeinfo']) && $request['afsender_ticket_kundeinfo'] != '') {
            $note_ticket_author = $user_id = $request['afsender_ticket_kundeinfo'];

        }
        $ticket_booking_id = '';
        if (isset($request['ticket_booking_id']) && $request['ticket_booking_id'] != '') {
            $ticket_booking_id = $request['ticket_booking_id'];
        }

        $ticket_tilmeldt = $request['ticket_tilmeldt'];
        $ticket_prioritering = $request['ticket_prioritering'];
        $ticket_oprettet_til = $request['kunde_ansvarlig'];
        $ticket_vedr = $request['ticket_vedr'];

        if ($ticket_oprettet_til != 0 && count($ticket_errors) == 0) {
            $data = array(
                'ticket_navn' => $ticket_overskrift,
                'ticket_type' => 3, //3: ny ticket
                'ticket_kunde_id' => $ticket_kunde_id,
                'ticket_tilmeldt' => $ticket_tilmeldt,
                'ticket_vedr' => $ticket_vedr,
                'ticket_prioritering' => $ticket_prioritering,
                'ticket_oprettet' => time(),
                'ticket_oprettet_af' => $user_id,
                'ticket_tlf' => $ticket_tlf,
                'ticket_adresse' => $ticket_adresse,
                'ticket_booking_id' => $ticket_booking_id,
                'ticket_oprettet_til' => $ticket_oprettet_til
            );

            // Save the posted value in the database
            $wpdb->insert($wpdb->prefix.'ticket', $data);

            $ticket_id = $wpdb->insert_id;
            $data_note = array(
                'note_kind' => 1,
                'note_reg_id' => $ticket_booking_id, // booking_id
                'author_kind' => 2,
                'author_id' => $note_ticket_author,
                'note' => $ticket_note,
                'datestamp' => time(),
            );

            $wpdb->insert($wpdb->prefix.'noter_booking', $data_note);

            $status = 'success';
            $message = 'Ticket er nu oprettet!';
        }
        else {
            $status = 'error';
            $message = 'Telefon skal indeholde 8 cifre!';
        }

        $return = ['status' => $status, 'message' => $message];

        return $return;
    }

}