<?php

class BookingModel
{

    public function getBookingByUser($request)
    {
        global $wpdb;

        $booking = $wpdb->get_row("
            SELECT *
            FROM wp_bookings
            WHERE user_id = " . (int) $request['user_id'] . "
                AND (booking_start < " . time() . " AND booking_slut > " . time() . ");",
        ARRAY_A);

        /*$booking = $wpdb->get_row("
            SELECT *
            FROM wp_bookings
            WHERE (booking_start < " . time() . " AND booking_slut > " . time() . ");",
            ARRAY_A);*/

        return $booking;
    }

} 