<?php

class CheckerModel
{

    protected $client;

    protected $server_name;

    public function __construct()
    {
        $this->client = new GuzzleHttp\Client();

        $this->server_name = 'http://' . $_SERVER['SERVER_NAME'];
    }

    public function getAll()
    {
        global $wpdb;

        $data = [];

        $checker = $wpdb->get_results("
            SELECT *
            FROM lb_checker lbc
            INNER JOIN  wp_users wpu
	            ON wpu.ID = lbc.user_id;
            ", ARRAY_A);

        if (is_array($checker) || is_object($checker))
        {
            foreach ($checker as $ch) {
                $ch['total_time'] = $this->getTotalTime($ch['check_in'], $ch['check_out']);
                $ch['distance'] = $this->getDistance(floatval($ch['checkin_lat']), floatval($ch['checkin_long']), floatval($ch['checkout_lat']), floatval($ch['checkout_long']), "K");

                $data[] = $ch;
            }
        }

        return $data;
    }

    private function getTotalTime($in, $out)
    {
        $diff = (strtotime($out) - strtotime($in));
        $total = $diff / 60;

        $hours = floor($total / 60);
        $minutes = $total % 60;

        return ($out != null) ? $hours.'h '.$minutes.'m' : '';
    }

    private function getDistance($lat1, $lon1, $lat2, $lon2, $unit) {
        if ($lat2 == null) {
            return null;
        }

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "M") {
            return (number_format($miles * 6371000,2)) . " Meters";
        }
        else if ($unit == "K") {
            return (number_format($miles * 1.609344,2)) . " Kilometers";
        }
        else if ($unit == "N") {
            return (number_format($miles * 0.8684,2)) . " Nautical Miles";
        }
        else {
            return $miles . " Miles";
        }
    }

    public function checkIn($request)
    {
        global $wpdb;

        // Get authenticated user
        $json = $this->client->get($this->server_name . '/wp-json/wp/v2/users/me?_envelope&access_token=' . $request['access_token']);
        $user = json_decode($json->getBody());

        $data = [
            'user_id' => $user->body->id,
            'checkin_long' => $request['checkin_long'],
            'checkin_lat' => $request['checkin_lat'],
            'check_out' => null,
            'booking_id' => $request['booking_id']
        ];

        $wpdb->insert('lb_checker', $data);

        return $this->getLogById($wpdb->insert_id);
    }

    public function addLog($request)
    {
        global $wpdb;

        $data = [
            'user_id' => $request['user_id'],
            'check_in' => date('Y-m-d H:i:s', strtotime($request['check_in'])),
            'checkin_lat' => $request['checkin_lat'],
            'checkin_long' => $request['checkin_long'],
            'check_out' => ( ! is_null($request['check_out'])) ? date('Y-m-d H:i:s', strtotime($request['check_out'])) : null,
            'checkout_lat' => ( ! is_null($request['check_out'])) ? $request['checkout_lat'] : null,
            'checkout_long' => ( ! is_null($request['check_out'])) ? $request['checkout_long'] : null,
            'zoom_level' => $request['zoom_level'],
            'status' => ( ! empty($request['check_out'])) ? 'logged' : 'ongoing'
        ];

        $wpdb->insert('lb_checker', $data);

        return $wpdb->get_row( "SELECT * FROM lb_checker WHERE id = $wpdb->insert_id", ARRAY_A );
    }

    public function checkOut(WP_REST_Request $request)
    {
        global $wpdb;

        // Get authenticated user
        $json = $this->client->get($this->server_name . '/wp-json/wp/v2/users/me?_envelope&access_token=' . $request['access_token']);
        $user = json_decode($json->getBody());

        $query = $wpdb->update(
            'lb_checker',
            array(
                'check_out' => date("Y-m-d H:i:s"),
                'checkout_long' => $request['checkout_long'],
                'checkout_lat' => $request['checkout_lat'],
                'status' => 'logged'
            ),
            array( 'id' => $request['log_id'] )
        );

        return ['query' => $query, 'log_data' => $this->getLogById($request['log_id'])];
    }

    public function getLogById($id)
    {
        global $wpdb;

        return $wpdb->get_row( "SELECT * FROM lb_checker WHERE id = $id", ARRAY_A );
    }

    public function getLogByToken(WP_REST_Request $request)
    {
        global $wpdb;

        // Get authenticated user
        $json = $this->client->get($this->server_name . '/wp-json/wp/v2/users/me?_envelope&access_token=' . $request['access_token']);
        $user = json_decode($json->getBody());

        $checker = $wpdb->get_row("
            SELECT *
            FROM lb_checker lbc

            INNER JOIN  wp_users wpu
	            ON wpu.ID = lbc.user_id

            WHERE lbc.user_id = " . (int) $user->body->id . " ORDER BY lbc.id DESC LIMIT 1;", ARRAY_A);

        if ($checker)
        {
            $checker['total_time'] = $this->getTotalTime($checker['check_in'], $checker['check_out']);
        }

        return $checker;
    }

    public function getLogByBookingId($request)
    {
        global $wpdb;

        $log = $wpdb->get_row( "SELECT * FROM lb_checker WHERE booking_id = " . $request['booking_id'], ARRAY_A );

        if (is_array($log)) {
            $log['distance'] = $this->getDistance(floatval($log['checkin_lat']), floatval($log['checkin_long']), floatval($log['checkout_lat']), floatval($log['checkout_long']), "K");
        }

        return $log;
    }

    public function getLog(WP_REST_Request $request)
    {
        global $wpdb;

        $checker = $wpdb->get_row("
            SELECT *
            FROM lb_checker lbc
            INNER JOIN  wp_users wpu
	            ON wpu.ID = lbc.user_id
            WHERE lbc.id = " . $request['id'] . ";");

        return $checker;
    }

    public function updateLog(WP_REST_Request $request)
    {
        global $wpdb;

        return $wpdb->update(
            'lb_checker',
            [
                'check_in' => date('Y-m-d H:i:s', strtotime($request['check_in'])),
                'checkin_lat' => $request['checkin_lat'],
                'checkin_long' => $request['checkin_long'],
                'check_out' => ( ! is_null($request['check_out'])) ? date('Y-m-d H:i:s', strtotime($request['check_out'])) : null,
                'checkout_lat' => ( ! is_null($request['check_out'])) ? $request['checkout_lat'] : null,
                'checkout_long' => ( ! is_null($request['check_out'])) ? $request['checkout_long'] : null,
                'zoom_level' => $request['zoom_level'],
                'status' => 'logged'
            ],
            [ 'id' => $request['id'] ]
        );
    }

} 