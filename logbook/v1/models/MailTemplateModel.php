<?php

class MailTemplateModel {

    public function getAll()
    {
        global $wpdb;

        $templates = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "mail_templates;");

        return $templates;
    }

    public function getTemplateById($id)
    {
        global $wpdb;

        $template = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "mail_templates WHERE id = " . $id . ";");

        return $template;
    }

    public function updateTemplate($request)
    {
        global $wpdb;

        $template = $wpdb->update(
            $wpdb->prefix . "mail_templates",
            [
                'from_name' => $request['from_name'],
                'from_email' => $request['from_email'],
                'subject' => $request['subject'],
                'mail_body' => $request['mail_body']
            ],
            ['id' => $request['id']]
        );

        return $template;
    }

} 