<?php

class CustomersController extends WP_REST_Controller
{
    protected $customer_model;

    public function __construct()
    {
        $this->customer_model = new CustomerModel();
    }

    public function register_routes()
    {
        $version = '1';
        $namespace = 'logbook/v' . $version;
        $base = 'customer';

        register_rest_route($namespace, '/' . $base . '/get_customer_by_booking_id', [
            [
                'methods'         => 'GET',
                'callback'        => [$this, 'getCustomerByBookingId']
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);

        register_rest_route($namespace, '/' . $base . '/post_end_job', [
            [
                'methods'         => 'POST',
                'callback'        => [$this, 'postEndJob']
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);

        register_rest_route($namespace, '/' . $base . '/make_note', [
            [
                'methods'         => 'POST',
                'callback'        => [$this, 'makeNote']
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);
    }

    public function getCustomerByBookingId($request)
    {
        $data = $this->customer_model->getCustomerByBookingId($request);

        return new WP_REST_Response( $data, 200 );
    }

    public function postEndJob($request)
    {
        $this->customer_model->postEndJob($request);

        $response = ['message' => 'Booking er nu opdateret!'];

        return new WP_REST_Response( $response, 200 );
    }

    public function makeNote($request)
    {
        $response = $this->customer_model->makeNote($request);

        return new WP_REST_Response( $response, 200 );
    }

} 