<?php

class AuthController extends WP_REST_Controller
{

    public $server_name;

    protected $client;

    public $client_id;

    public $client_secret;

    public function __construct()
    {
        $this->server_name = 'http://' . $_SERVER['SERVER_NAME'];

        $this->client = new GuzzleHttp\Client();

        //$this->client_id = 'jWeQeOwhE7hrUkCyPp6lMPQ1BzrjD5'; // local
        $this->client_id = 'rmao73q4CDyppcUaXhqY8ulbIq9Mx0'; // live
        //$this->client_id = '509xlWLiLxLkKYdmNhHMS1cay1FsTB'; // live 2

        //$this->client_secret = 'ZqHl44bPLuTuHocNX0VK71pMhGwS3H'; // local
        $this->client_secret = '6YnhmulNyXr7mRjg1hyrfHWBSE11ry'; // live
        //$this->client_secret = 'YZC7xX4igPQ19A0nXNEEAwGMHeOnGH'; // live 2
    }

    public function register_routes()
    {
        $version = '1';
        $namespace = 'logbook/v' . $version;
        $base = 'app';

        register_rest_route($namespace, '/' . $base . '/login', [
            [
                'methods'   => 'POST',
                'callback'  => [$this, 'doLogin']
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);

        register_rest_route($namespace, '/' . $base . '/logout', [
            [
                'methods'   => 'POST',
                'callback'  => [$this, 'doLogout']
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);

        register_rest_route($namespace, '/' . $base . '/get_user', [
            [
                'methods'   => 'GET',
                'callback'  => [$this, 'getUser']
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);
    }

    public function doLogin($request)
    {
        $auth = base64_encode($this->client_id . ':' . $this->client_secret);

        try {
            $json = $this->client->post($this->server_name . '?oauth=token', [
                'auth' => [
                    $this->client_id,
                    $this->client_secret
                ],
                'form_params' => [
                    'grant_type' => 'password',
                    'username' => $request['username'],
                    'password' => $request['password']
                ]
            ]);

            $json_response = json_decode($json->getBody());

            return new WP_REST_Response( $json_response, 200 );
        }
        catch (Exception $e) {
            return $e;
        }
    }

    public function doLogout($request)
    {
        try {
            $json = $this->client->post($this->server_name . '/oauth/destroy?access_token=' . $request['access_token'] . '&refresh_token=' . $request['refresh_token']);
            $json = json_decode($json->getBody());

            return new WP_REST_Response( $json, 200 );
        }
        catch (Exception $e) {
            return $e;
        }
    }

    public function getUser($request)
    {
        $user = $this->client->get($this->server_name . '/wp-json/wp/v2/users/me?_envelope&access_token=' . $request['access_token']);

        $json = json_decode($user->getBody());

        return new WP_REST_Response( $json, 200 );
    }

}