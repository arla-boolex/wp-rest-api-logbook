<?php

class CheckerController extends WP_REST_Controller
{
    protected $checker_model;

    public function __construct()
    {
        $this->checker_model = new CheckerModel();
    }

    public function register_routes()
    {
        $version = '1';
        $namespace = 'logbook/v' . $version;
        $base = 'checker';

        register_rest_route($namespace, '/' . $base, [
            [
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => [$this, 'get_items']
            ],
            array(
                'methods'               => WP_REST_Server::CREATABLE,
                'callback'              => array( $this, 'create_item' ),
                'args'                  => array_merge( $this->get_endpoint_args_for_item_schema( WP_REST_Server::CREATABLE ), array(
                    'user_id'           => array(
                        'required'      => true
                    ),
                ) ),
            ),

            'schema' => array( $this, 'get_public_item_schema' )
        ]);

        register_rest_route($namespace, '/' . $base . '/checkin', [
            array(
                'methods'               => 'POST',
                'callback'              => array( $this, 'checkIn' ),
                'args'                  => array_merge( $this->get_endpoint_args_for_item_schema( WP_REST_Server::CREATABLE ) )
            ),

            'schema' => array( $this, 'get_public_item_schema' )
        ]);

        register_rest_route($namespace, '/' . $base . '/(?P<id>[\d]+)', [
            [
                'methods'         => WP_REST_Server::READABLE,
                'callback'        => [$this, 'get_item']
            ],
            [
                'methods'         => WP_REST_Server::EDITABLE,
                'callback'        => array( $this, 'update_item' ),
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);

        register_rest_route($namespace, '/' . $base . '/(?P<id>[\d]+)/checkout', [
            [
                'methods'         => 'POST',
                'callback'        => [$this, 'checkOut'],
                'args'            => $this->get_endpoint_args_for_item_schema( WP_REST_Server::EDITABLE )
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);

        register_rest_route($namespace, '/' . $base . '/(?P<id>[\d]+)/get_by_token', [
            [
                'methods'         => 'GET',
                'callback'        => [$this, 'getLogByToken']
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);

        register_rest_route($namespace, '/' . $base . '/(?P<id>[\d]+)/get_log_by_booking_id', [
            [
                'methods'         => 'GET',
                'callback'        => [$this, 'getLogByBookingId']
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);
    }

    public function get_items($request)
    {
        $items = $this->checker_model->getAll();

        return new WP_REST_Response( $items, 200 );
    }

    public function checkIn($request)
    {
        $data = $this->checker_model->checkIn($request);

        if (is_array($data)) {
            return new WP_REST_Response($data, 200);
        }

        return new WP_Error('cant-create', __('The system is refusing from checking you in. Please contact administrator.', 'text-domain'), ['status' => 500]);
    }

    public function create_item($request) {
        $data = $this->checker_model->addLog($request);

        if (is_array($data)) {
            return new WP_REST_Response($data, 200);
        }

        return new WP_Error('cant-create', __('The system is refusing from creating an item. Please contact administrator.', 'text-domain'), ['status' => 500]);
    }

    public function checkOut($request)
    {
        $data = $this->checker_model->checkOut($request);

        if ( 0 === $data['query'] || false === $data['query'] ) {
            return new WP_Error( 'cant-update', __( 'The system is refusing from checking you out. Please contact administrator.', 'text-domain'), array( 'status' => 500 ) );
        }

        return new WP_REST_Response( $data['log_data'], 200 );
    }

    public function getLogByToken($request)
    {
        $data = $this->checker_model->getLogByToken($request);

        return new WP_REST_Response( $data, 200 );
    }

    public function getLogByBookingId($request)
    {
        $log = $this->checker_model->getLogByBookingId($request);

        return new WP_REST_Response( $log, 200 );
    }

    public function get_item($request)
    {
        $data = $this->checker_model->getLog($request);

        if ( $data ) {
            return new WP_REST_Response( $data, 200 );
        }else{
            return new WP_Error( 'code', __( 'The system is refusing from getting an item. Please contact administrator.', 'text-domain' ) );
        }
    }

    public function update_item( $request ) {
        $data = $this->checker_model->updateLog($request);

        if ( $data ) {
            return new WP_REST_Response( $data, 200 );
        }

        return new WP_Error( 'cant-update', __( 'The system is refusing from updating an item. Please contact administrator.', 'text-domain'), array( 'status' => 500 ) );

    }

}