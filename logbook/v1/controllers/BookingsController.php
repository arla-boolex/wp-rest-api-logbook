<?php

class BookingsController extends WP_REST_Controller
{

    protected $booking_model;

    public function __construct()
    {
        $this->booking_model = new BookingModel();
    }

    public function register_routes()
    {
        $version = '1';
        $namespace = 'logbook/v' . $version;
        $base = 'booking';

        register_rest_route($namespace, '/' . $base . '/get_booking_by_user', [
            [
                'methods'         => 'GET',
                'callback'        => [$this, 'getBookingByUser']
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);
    }

    public function getBookingByUser($request)
    {
        $booking = $this->booking_model->getBookingByUser($request);

        return new WP_REST_Response( $booking, 200 );
    }

} 