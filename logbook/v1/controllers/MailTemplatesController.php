<?php

class MailTemplatesController extends WP_REST_Controller
{

    protected $mail_template_model;

    public function __construct()
    {
        $this->mail_template_model = new MailTemplateModel();
    }

    public function register_routes()
    {
        $version = '1';
        $namespace = 'logbook/v' . $version;
        $base = 'mail_templates';

        register_rest_route($namespace, '/' . $base, [
            array(
                'methods'               => 'GET',
                'callback'              => [ $this, 'getItems' ]
            ),

            'schema' => array( $this, 'get_public_item_schema' )
        ]);

        register_rest_route($namespace, '/' . $base . '/(?P<id>\d+)', [
            [
                'methods'         => WP_REST_Server::READABLE,
                'callback'        => [$this, 'getItem']
            ],
            [
                'methods'         => WP_REST_Server::EDITABLE,
                'callback'        => array( $this, 'updateItem' ),
            ],

            'schema' => array( $this, 'get_public_item_schema' )
        ]);
    }

    public function getItems()
    {
        $templates = $this->mail_template_model->getAll();

        return new WP_REST_Response( $templates, 200 );
    }

    public function getItem($data)
    {
        $template = $this->mail_template_model->getTemplateById($data['id']);

        return new WP_REST_Response( $template, 200 );
    }

    public function updateItem($request)
    {
        $template = $this->mail_template_model->updateTemplate($request);

        if ( $template ) {
            return new WP_REST_Response( $template, 200 );
        }

        return new WP_Error( 'cant-update', __( 'The system is refusing from updating an item. Please contact administrator.', 'text-domain'), array( 'status' => 500 ) );
    }

} 