<?php

class ExportExcelController extends WP_REST_Controller
{

    protected $checker_model;

    public function __construct()
    {
        $this->checker_model = new CheckerModel();
    }

    public function register_routes()
    {
        $version = '1';
        $namespace = 'logbook/v' . $version;
        $base = 'excel';

        register_rest_route($namespace, '/' . $base . '/export', [
            array(
                'methods'               => 'GET',
                'callback'              => array( $this, 'exportNow' )
            ),

            'schema' => array( $this, 'get_public_item_schema' )
        ]);
    }

    public function exportNow()
    {
        // Create a new PHPExcel Object
        $objPHPExcel = new PHPExcel();

        // Set header values of the worksheet
        $this->setWorksheetHeader($objPHPExcel);

        // Get data and populate the worksheet
        $data = $this->populateWorksheetWithData($objPHPExcel);

        // Set spreadsheet metadata
        $this->setMetadata($objPHPExcel);

        // Set worksheet style
        $this->setWorksheetStyle($objPHPExcel, $data);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="LykkeboDetails.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }

    public function setWorksheetHeader(PHPExcel $objPHPExcel)
    {
        $headerData = ["User ID", "Name", "Check-in\n(DD/MM/YYYY)", "Check-out\n(DD/MM/YYYY)", "Total Time", "Distance"];

        $objPHPExcel->getActiveSheet()->fromArray($headerData, NULL);
    }

    public function setMetadata(PHPExcel $objPHPExcel)
    {
        $objPHPExcel->getProperties()->setCreator('Lykkebo Admin');
        $objPHPExcel->getProperties()->setTitle('User Logs (Check-in & Check-out');
    }

    public function setWorksheetStyle(PHPExcel $objPHPExcel, $data)
    {
        // Set worksheet's page orientation and size
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER_SMALL);

        // Set alignment
        $style_array = [
            'alignment' => [
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ],
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => [
                    'argb' => 'DDDDDD'
                ]
            ]
        ];
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($style_array);
        $objPHPExcel->getActiveSheet()->getStyle('A1:A' . (count($data) + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        // Wrap text to all active cells
        $objPHPExcel->getActiveSheet()->getStyle('A1:F' . (count($data) + 1))->getAlignment()->setWrapText(true);

        // Set column widths
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
    }

    public function populateWorksheetWithData(PHPExcel $objPHPExcel)
    {
        $data = [];

        $logs = $this->checker_model->getAll();

        if ($logs) {
            foreach ($logs as $log) {
                $data[] = [
                    $log['user_id'],
                    $log['user_login'],
                    $log['check_in'] != null ? date('d/m/Y h:i A', strtotime($log['check_in'])) : '',
                    $log['check_out'] != null ? date('d/m/Y h:i A', strtotime($log['check_out'])) : '',
                    $log['total_time'],
                    $log['distance']
                ];
            }
        }

        $objPHPExcel->getActiveSheet()->fromArray($data, NULL, 'A2');

        return $data;
    }

} 